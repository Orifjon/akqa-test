﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using WebApp.Converters;

namespace WebApp.Tests.Converters
{
    [TestFixture]
    public class NumberConverterTests
    {
        [TestCase(-1, "minus one")]
        [TestCase(0, "zero")]
        [TestCase(1, "one")]
        [TestCase(19, "nineteen")]
        [TestCase(10, "ten")]
        [TestCase(20, "twenty")]
        [TestCase(99, "ninety-nine")]
        [TestCase(90, "ninety")]
        [TestCase(100, "one hundred")]
        [TestCase(101, "one hundred and one")]
        [TestCase(999, "nine hundred and ninety-nine")]
        [TestCase(1000, "one thousand")]
        [TestCase(1001, "one thousand one")]
        [TestCase(1000000, "one million")]
        public void Convert_ShouldHandleNumbers(int number, string expected)
        {
            //arrange
            var sut = new NumberConverter();

            //act
            var result = sut.Convert(number);

            //assert
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Convert_ShouldThrow_WhenNumberIsBig()
        { 
            //arrange
            var sut = new NumberConverter();

            //act and assert
            Assert.Throws<ArgumentOutOfRangeException>(()=> sut.Convert(2_147_483_647));
        }

    }
}
