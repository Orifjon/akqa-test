﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using WebApp.Converters;

namespace WebApp.Tests.Converters
{
    [TestFixture]
    public class MoneyConverterTests
    {
        [Test]
        public void Convert_ShouldUseDollars()
        {
            // arrange
            var innerConverter = Substitute.For<IIntegerConverter>();
            innerConverter.Convert(0).ReturnsForAnyArgs("number");
            innerConverter.Convert(2).Returns("two");

            var sut = new MoneyConverter(innerConverter);

            // act
            var result = sut.Convert(2.5);

            // assert
            Assert.AreEqual("two dollars and number cents", result);

        }
    }
}
