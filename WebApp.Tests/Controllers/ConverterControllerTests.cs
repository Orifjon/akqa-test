﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using WebApp.Controllers;
using WebApp.Converters;

namespace WebApp.Tests.Controllers
{
    [TestFixture]
    public class ConverterControllerTests
    {
        [Test]
        public void Get()
        {
            // arrange
            var converter = Substitute.For<IDoubleConverter>();
            converter.Convert(0).ReturnsForAnyArgs("two dollars");
            var sut = new ConverterController(converter);

            // act
            var result = sut.Get("John", 2);

            // assert
            Assert.AreEqual("John", result.Name);
            Assert.AreEqual("two dollars", result.Text);
        }
    }
}
