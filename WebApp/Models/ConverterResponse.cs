﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    public class ConverterResponse
    {
        public string Name { get; set; }
        public string Text { get; set; }
    }
}