﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApp.Converters;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class ConverterController : ApiController
    {
        private IDoubleConverter _converter;

        public ConverterController(IDoubleConverter converter)
        {
            _converter = converter;
        }

        // GET api/<controller>?name={name}&number={number}
        public ConverterResponse Get([FromUri] string name, [FromUri]double number )
        {
            return new ConverterResponse {Name = name, Text = _converter.Convert(number)};
        }

    }
}