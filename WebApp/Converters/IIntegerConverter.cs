﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.Converters
{
    public interface IIntegerConverter
    {
        string Convert(int number);
    }
}
