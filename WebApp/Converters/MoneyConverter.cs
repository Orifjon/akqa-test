﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Converters
{
    public class MoneyConverter: IDoubleConverter
    {
        private readonly IIntegerConverter _numberConverter;

        public MoneyConverter(IIntegerConverter numberConverter)
        {
            _numberConverter = numberConverter;
        }

        public string Convert(double number)
        {
            // TODO: add checks for 0, 1 dollars/cents
            var dollars = (int)Math.Floor(number);
            var cents = (int)Math.Floor((number - dollars) * 100);

            var dollarText = _numberConverter.Convert(dollars);
            var centsText = _numberConverter.Convert(cents);

            return $"{dollarText} dollars and {centsText} cents";
        }
    }
}