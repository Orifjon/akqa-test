﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Converters
{
    public class NumberConverter: IIntegerConverter
    {
        private static string unitSeparator = "-";
        private static string tenSeparator = " and ";
        private static string[] units = {
            "", "one", "two", "three", "four", "five", "six", "seven",
            "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen",
            "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"
        };

        private static string[] tens = {
            "",  "", "twenty", "thirty", "forty", "fifty", "sixty",
            "seventy", "eighty", "ninety"
        };

        private static List<Scale> scales = new List<Scale>
        {
            new Scale(3, "thousand"),
            new Scale(6, "million"),
            //new Scale(9, "billion")
        };


        public string Convert(int number)
        {
            if (number == 0)
            {
                return "zero";
            }

            if (number < 0)
            {
                return "minus " + DoConvert(-number);
            }

            return DoConvert(number);
        }

        private string DoConvert(int number)
        {
            if (number < 20)
            {
                return units[number];
            }

            if (number < 100)
            {
                var tenDigit = number / 10;
                var unit = number % 10;
                var separator = unit == 0 ? "" : unitSeparator;
                return tens[tenDigit] + separator + units[unit];
            }

            if (number < 1000)
            {
                var hundredDigit = number / 100;
                var separator = number % 100 == 0 ? "" : tenSeparator;
                return units[hundredDigit] + " hundred" + separator + DoConvert(number % 100);
            }

            foreach (var scale in scales)
            {
                // beware rounding errors
                var upper = (int)Math.Pow(10, scale.Exponent + 3);
                if (number < upper)
                {
                    // beware rounding errors
                    var lower = (int)Math.Pow(10, scale.Exponent);
                    var separator = number % lower != 0 ? scale.Separator : "";
                    return DoConvert(number / lower) + " "+ scale.Name
                        + separator 
                        + DoConvert(number % lower);
                }
            }

            throw new ArgumentOutOfRangeException(nameof(number), number, "Number is too big to convert");
        }

        private class Scale
        {
            public int Exponent { get; }
            public string Name { get; }
            public string Separator { get; }

            public Scale(int exponent, string name, string separator = " ")
            {
                Exponent = exponent;
                Name = name;
                Separator = separator;
            }
        }
    }
}